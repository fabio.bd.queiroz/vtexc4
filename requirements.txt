robotframework==3.2.1
robotframework-faker==5.0.0
robotframework-seleniumlibrary==4.4.0
robotframework-xvfb==1.2.2
robotframework-metrics==3.1.6
robotframework-requests==0.7.0