*** Settings ***
Documentation     Automação de testes Projeto Novo Ecom C4 X Vtex
Library           SeleniumLibrary
Suite teardown    Close all browsers

*** Variables ***
${LOGIN URL}                https://carrefourbrqa.vtexcommercestable.com.br/checkout/cart/add?sku=310119818&qty=1&seller=1&redirect=true&sc=1
${BROWSER}                  Chrome
${FECHAR}                   xpath:(//a[@data-i18n='cart.finalize'])[1]
${EMAIL}                    id:client-pre-email    
${BTN_EMAIL}                id:btn-client-pre-email  
${URL_EMAIL}                https://carrefourbrqa.vtexcommercestable.com.br/checkout/#/email          
${BTN_EXISTE_EMAIL}         id:btn-identified-user-button
${BTN_RECEBER}              id:shipping-option-delivery
${BTN_PAGAMENTO}            id:btn-go-to-payment
${BT_AGENDADA}              id:scheduled-delivery-choose-Agendada
${DIA_AGENDA}               xpath://div[contains(@class,'day--keyboard-selected')]
${TITULO_PAGAMENTO}         xpath:(//span[contains(.,'Pagamento')])[2]
${BTN_FINALIZAR}            xpath://button[@id="payment-data-submit"][contains(.,'Finalizar compra')]
${MSG_CONFIRMADO}           xpath://h1[contains(.,'Pedido Confirmado')]
${ID_ORDER}                 id:order-id
${Pedido}

*** Test Cases ***
Fechar compra 1p
    Open Browser To Login Page



*** Keywords ***
Open Browser To Login Page
    Open Browser                       ${LOGIN URL}    ${BROWSER}
    Title Should Be                    Finalizar compra
    Wait Until Element Is Visible      ${FECHAR} 
    Go To                              ${URL_EMAIL}                            
    # Click Element                      ${FECHAR}        action_chain=True  
    # Execute JavaScript                  ${FECHAR}.click();
    # Sleep       5
    Wait Until Element Is Visible       ${EMAIL}         timeout=20s
    Click Element                      ${EMAIL}  
    Input Text                         ${EMAIL}     fabio_queiroz_ext@carrefour.com  
    Wait Until Element Is Visible       ${BTN_EMAIL}
    Click Button                       ${BTN_EMAIL}
    Capture Page Screenshot            
    Wait Until Element Is Visible      ${BTN_EXISTE_EMAIL}      timeout=20s
    Click Button                       ${BTN_EXISTE_EMAIL} 
    Wait Until Page Contains Element     ${BTN_RECEBER}         timeout=20s      
    Click Element                      ${BTN_RECEBER} 
    Wait Until Element Is Visible       ${BT_AGENDADA}
    Click Element                       ${BT_AGENDADA}
    Click Element                       ${DIA_AGENDA}  
    Click Button                       ${BTN_PAGAMENTO}  
    Capture Page Screenshot 
    Element Should Contain              ${TITULO_PAGAMENTO}    Pagamento
    Wait Until Element Is Visible        ${BTN_FINALIZAR}       timeout=20s  
    Sleep                               5 
    Click Button                        ${BTN_FINALIZAR} 
    Capture Page Screenshot 
    Sleep                               7 
    Page Should Contain                 Seu pedido foi processado com sucesso
    ${Pedido}                           Get Text       ${ID_ORDER}
    Log To Console                      Pedido numero: ${Pedido}                 
    Capture Page Screenshot 







