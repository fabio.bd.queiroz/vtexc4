# Vtexc4

Projeto de Automação de Testes


# Instalar  python
WIN 
https://python.org.br/instalacao-windows/
Linux (geralmente ja vem instalado)
sudo apt-get install python3

# Update pip
python -m pip install --upgrade pip

# Instalar  robot
https://robotframework.org/

pip install robotframework

# Instalar  libraries
https://robotframework.org/#libraries

# Gerar dados fakes
pip install robotframework-faker

# Testes Web
pip install --upgrade robotframework-seleniumlibrary

# Testes Servico (API)
pip install robotframework-requests

config vscode

http://robotizandotestes.blogspot.com/2020/02/season-editores-ep-02-visual-studio.html


# Baixar drivers do chrome  e firefox

pip install webdrivermanager
webdrivermanager firefox chrome --linkpath C:\Python\Scripts


# Intalar metrics para geração de Reports
https://github.com/xNok/robotframework-metrics


# Run tests

robot -d  results checkout.robot && robotmetrics --inputpath ./Results/ --output output.xml --log log.html --logo "https://static.carrefour.com.br/imagens/home-carrefour/img/logoCarrefourSite.svg"



